import Vapor

/// Register your application's routes here.
public func routes(_ router: Router) throws {
    // Basic "Hello, world!" example
    router.get("hello") { req in
        return "Hello, world!"
    }

    router.post("api", "ops") { req -> Future<Delta> in
        return try req.content.decode(Delta.self).flatMap(to: Delta.self) { delta in
            return delta.save(on: req)
        }
    }
}
