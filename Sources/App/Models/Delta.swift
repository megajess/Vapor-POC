//
//  Delta.swift
//  App
//
//  Created by Jesse Suter on 5/9/18.
//

import Vapor
import FluentSQLite

final class Delta: SQLiteModel, Migration, Codable, Content {
    var id: Int?
    var ops: String
    
    init(ops: String) {
        self.ops = ops
    }
}
